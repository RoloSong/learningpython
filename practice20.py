#http://www.runoob.com/python/python-exercise-example20.html
class ball:
    def __init__(self,initheight):
        self.height = initheight
        self.bounceCount = 0
        self.mileage = 0
    def bounce(self,count):
        for self.bounceCount in range(0,count):
            self.mileage += self.height
            self.height /= 2
            self.mileage += self.height

initHeight = int(input('Please input the initial height of the ball'))
bounceLimit = int(input('Please input the bounce time limitation of the ball'))
currentBall = ball(initHeight)
currentBall.bounce(bounceLimit)
print('The current height of the ball is %f, the add up mileage of the ball is %f'
      % (currentBall.height,currentBall.mileage))
