#http://www.runoob.com/python/python-exercise-example19.html




def rd(n):
    list = []
    if n < 2:
        list.append(n)
        return list

    for a in range(1,n):
        if n%a == 0:
            list.append(a)
    return list



def judge_perfect(num,list):
    temp = 0
    for a in list:
        temp += a
    if int(num)-int(temp) == 0:
        return 1
    return 0

def call_func_list(num):
    l = rd(num)
    #print(l)
    return judge_perfect(num,l)
#main
for a in range(1,1000):
    if call_func_list(a):
        print(a)

