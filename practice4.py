#http://www.runoob.com/python/python-exercise-example4.html
import copy
year = int(input('Input Year'))
month = int(input('Input Month'))
day = int(input('Input Day'))

month_day_count = [31,28,31,30,31,30,31,31,30,31,30,31]
total = 0

if year%4 == 0:
    month_day_count[1] = 29

for mon in range(0,month-1):
    total += month_day_count[mon]

total += day

print(total)