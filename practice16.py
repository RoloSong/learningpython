#http://www.runoob.com/python/python-exercise-example16.html
import datetime
if __name__ == '__main__':
    print(datetime.date.today().strftime('%Y-%m-%d'))

    birthdate = datetime.date(1995,10,7)
    print(birthdate.strftime('%Y-%m-%d'))

    birthdateNextDay = birthdate + datetime.timedelta(days = 1)
    print(birthdateNextDay.strftime('%Y-%m-%d'))

    birthdateNextYear = birthdate.replace(year = birthdate.year + 1)
    print('birthdateNextYear '+birthdateNextYear.strftime('%Y-%m-%d'))
    print('birthdateOriginal '+birthdate.strftime("%Y-%m-%d"))